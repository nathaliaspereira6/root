<?php
require_once('config.php');
// pesquisar outras formas de conexão mysql (mysqli)
// conexao banco utilizando PDO
// PHP DATA OBJECT
//$server = 'localhost';
$banco = 'aulaphpdb';
$usuario = 'root ';
$senha = '';

try {
    $conn = new PDO("mysql:host=".SERVIDOR_DB.
    ";dbname=".$banco,$usuario,$senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE,
    PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Erro: '.$e->getMessage();
}

?>