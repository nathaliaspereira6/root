#Criando banco de dados
create database aulaphpdb;

#definindo banco ativo
use aulaphpdb;

#criando tabela aluno (teste)
create table aluno(
    id int not null auto_increment,
    nome varchar(60) not null,
    cpf varchar(11) unique not null,
    email varchar(60) not null unique,
    data_cad timestamp default current_timestamp 
);
