<?php
require_once('conexao.php');
$query = "select * from banner";
$cmd = $cn->prepare($query);
$cmd->execute();
$banner_retornados = $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count($banner_retornados)>0)
{
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista banner</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="td_banner" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
    <tr bgcolor="#993300" align="center">
        <th width="15%" height="2" font size="2" color="fff">Código</th>
        <th width="15%" height="2" font size="2" color="fff">Banner</th>
        <th width="15%" height="2" font size="2" color="fff">link</th>
        <th width="15%" height="2" font size="2" color="fff">Imagem</th>
        <th width="15%" height="2" font size="2" color="fff">Alt</th>
        <th width="15%" height="2" font size="2" color="fff">Ativo</th>
    </tr>
    <?php
    foreach($banner_retornados as $banner){
    ?>
    <tr>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $banner['id_banner']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $banner['titulo_banner']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $banner['link_banner']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $banner['img_banner']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $banner['alt']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $banner['banner_ativo']=='1'?'sim':'não'; ?></font></td>
        <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Alterar</a></font></td>
        <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="<?php echo "op_banner.php?excluir=1&id=".$adm['id']?>">Excluir</a></font></td>
  </tr>
        <?php }} ?>
  </table>
</body>
</html>