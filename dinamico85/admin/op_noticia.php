<?php
require_once('../config.php');

if (isset($_POST['cadastro']))
{
    $imagem = upload_imagem($_FILES["imagem"]);
    $noticia= new Noticia
    (
        0,
        $_POST['id_categoria_noticia'],
        $_POST['titulo_noticia'],                     
        $imagem[0],               
        $_POST['data_noticia'],
        isset($_POST['check_ativo'])?'1':'0',
        $_POST['noticia']
    );
    
            $noticia->Insert();
            header('location:principal.php?link=6&msg=ok');
}
        //* Deletar 
        $id = filter_input(INPUT_GET,'id');
        $excluir = filter_input(INPUT_GET,'excluir');
        if(isset($id)&& $excluir==1)
        {
            $post = new Noticia();
            $post->setId($id);
            $post->delete();
            header('location:principal.php?link=7&msg=ok');
        }
        //* Alterar 
        if(isset($_POST['alterar']))
        {
            $post = new Noticia();
            $post->update
            (
                $_POST['id'],
                $_POST['titulo_noticia'],
                $_POST['noticia'],
                $_POST['img_noticia'],
                $_POST['visitas'],
                $_POST['data_noticia'],
                $_POST['check_ativo']
            );
            header('location:principal.php?link=7&msg=ok');
        }
?>