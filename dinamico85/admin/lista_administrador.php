
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista administradores</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="td_adm" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
    <tr bgcolor="#993300" align="center">
        <th width="15%" height="2" font size="2" color="fff">ID</th>
        <th width="15%" height="2" font size="2" color="fff">Nome</th>
        <th width="15%" height="2" font size="2" color="fff">Email</th>
        <th width="15%" height="2" font size="2" color="fff">Login</th>
    </tr>
    <?php
    require_once('../config.php');
    $adms = Administrador::getList();
    foreach($adms as $adm){
    ?>
    <tr>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $adm['id']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $adm['nome']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $adm['email']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $adm['login']?></font></td>
        <td align="center"><font size="2" face="verdana, arial" color="#fff">
            <a href="<?php echo "alterar_adm.php?id=".$adm['id']."&nome=".$adm['nome']."&email=".$adm['email']."&login=".$adm['login']?>">Alterar</a>
        </font></td>
        <td align="center"><font size="2" face="verdana, arial" color="#fff">
            
        <a href="<?php echo "op_administrador.php?excluir=1&id=".$adm['id']?>">Excluir</a>
    </font></td>
  </tr>
        <?php } ?>
  </table>
</body>
</html>