<?php
class categoria
{
    private $id_categoria;
    private $nome_categoria;
    private $ativo;



    // declaração de metodos de acesso
    public function getId()
        {
            return $this->id_categoria;
        }
        public function setId($value)
        {
            $this->id = $value;
        }
        //Método de acesso para Nome
        public function getNome()
        {
            return $this->nome;
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }
        
        //Método de acesso para Nome
        public function getAtivo()
        {
            return $this->ativo;
        }
        public function setAtivo($value)
        {
            $this->ativo = $value;
        }

    public function loadById($_id)
    {
    //sql - instancia da classe sql
        $sql = new Sql();
        $results= $sql-> select("SELECT * FROM categoria WHERE id_categoria = :id", array(':id'=>$_id));
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

    public static function getList()
    {
        $sql = new Sql();
    return $sql->select("SELECT * FROM categoria order by categoria");
    }

    public static function search($nome_categoria)
    {
        $sql = new Sql();
    return $sql->select("SELECT * FROM categoria WHERE nome LIKE :nome", array(":nome"=>"%".$nome_categoria."%"));
    }
    
  

    public function setData($data)
    {
        $this->setId($data['id']);
        $this->setNome($data['nome']);
        $this->setEmail($data['ativo']);

    }
    
    public function insert()
    {
        $sql = new Sql();
        $results = $sql->select('CALL sp_categoria_insert(:nome,:ativo)',
        array(':nome'=>$this->getNome(),
        ':ativo'=>$this->getAtivo()));
      
        
        if (count($results) > 0) 
        {
            $this->setData($results[0]);
        }            
    }

    public function update($_id, $_nome, $_ativo)
    {
        $sql = new Sql();
        $sql->query("UPDATE categoria SET nome= :nome, ativo = :ativo WHERE id = :id",
        array(
            ":id" => $_id,
            ":nome" =>$_nome,
            ":ativo" => $_ativo
           ));
    }
    
    public function delete()
    {
        $sql = new Sql();
        $sql->query("DELETE FROM categoria WHERE id= :id", array(":id"=>$this->getId()));
    }
    
    public function __constuct($_nome="",$_ativo="")
    {
        $this->nome = $_nome;
        $this->ativo = $_ativo;

    }
}
?>