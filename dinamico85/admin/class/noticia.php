<?php
    Class Noticia
    {
        
        public $id_noticia;
        public $id_categoria;
        public $noticia;
        public $titulo_noticia;
        public $img_noticia;
        public $visita_noticia;
        public $data_noticia;
        public $noticia_ativo;


        //* Id da Noticia
        public function getId()
        {
            return $this->id_noticia;
        }
        public function setId($value)
        {
            $this->id_noticia = $value;
        }
        //* Id da categoria
        public function getIdCategoria()
        {
            return $this->id_categoria;
        }
        public function setIdCategoria($value)
        {
            $this->id_categoria = $value;
        }
        //* Noticia
        public function getNoticia()
        {
            return $this->noticia;
        }
        public function setNoticia($value)
        {
            $this->noticia = $value;
        }
        //* Titulo da noticia
        public function getTitulo()
        {
            return $this->titulo_noticia;
        }
        public function setTitulo($value)
        {
            $this->titulo_noticia = $value;
        }
        //* Imagem da noticia
        public function getImgNoticia()
        {
            return $this->img_noticia;
        }
        public function setImgNoticia($value)
        {
            $this->img_noticia = $value;
        }
        //* Visitas na noticia
        public function getVisita()
        {
            return $this->visita_noticia;
        }
        public function setVistita($value)
        {
            $this->visita_noticia = $value;
        }
        //* Data de publicação da noticia
        public function getDataNoticia()
        {
            return $this->data_noticia;
        }
        public function setDataNoticia($value)
        {
            $this->data_noticia = $value;
        }
        //* Noticia ativa
        public function getNoticiaAtiva()
        {
            return $this->noticia_ativo;
        }
        public function setNoticiaAtiva($value)
        {
            $this->noticia_ativo = $value;
        }
    
        
        //* Buscar por id da noticia 
        public function BuscarPorId($_id)
        {
            $sql = new Sql();
            $resultado = $sql->select('SELECT * FROM noticias WHERE id_noticia = :id', 
            array
            (
                ':id' => $_id
            ));
            if(count($resultado)>0)
            {
                $this->setData($resultado[0]);
            }
        }
        //* Gerar Lista de noticias
        public static function GetList()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM noticias order by id_noticia');
        }
        //* Passando dados aos atributos
        public function setData($data)
        {
            $this->setId($data['id_noticia']);
            $this->setIdCategoria($data['id_categoria']);
            $this->setNoticia($data['noticia']);
            $this->setTitulo($data['titulo_noticia']);
            $this->setImgNoticia($data['img_noticia']);
            $this->setVistita($data['visita_noticia']);
            $this->setDataNoticia($data['data_noticia']);
            $this->setNoticiaAtiva($data['noticia_ativo']);
        }
        //* Inserir Noticia
        public function Insert()
        {
            $sql = new Sql();
            $$resultado = $sql->select('call sp_noticias_insert(:id_cat,:titulo,:img,0,:data,:ativo,:noticia)',
            array
            (
                ':id_cat'=>$this->getIdCategoria(),
                ':noticia'=>$this->getNoticia(),
                ':titulo'=>$this->getTitulo(),
                ':img'=>$this->getImgNoticia(),
                ':data'=>$this->getDataNoticia(),
                ':ativo'=>$this->getNoticiaAtiva()
            ));
            if(count($resultado)>0)
            {
                $this->setData($resultado[0]);
            }
        }
        //* Atualizar a noticia
        public function Update($_id_noticia,$_noticia, $_titulo, $_img, $_visita, $_data, $_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE noticias SET noticia = :not, titulo_noticia = :titulo, img_noticia = :img, visita_noticia = :visita, data_noticia = :dat, noticia_ativo = :ativ WHERE id = :id',
        array
        (
            ':id'=>$_id_noticia,
            ':not'=>$_noticia,
            ':titulo'=>$_titulo,
            ':img'=>$_img,
            ':visita'=>$_visita,
            ':data'=>$_data,
            ':ativ'=>$_ativo
        ));
        }

        //* Deletar a Noticia
        public function Deletar()
        {
            $sql = new Sql();
            $sql->query('DELETE * FROM noticias WHERE id = :id',
            array
            (
                ':id'=>$this->getId()
            ));
        }

        //* Contabilizat noticias
        public function UpdateVisita($id)
        {
            $sql = new Sql();
            $sql->query('UPDATE noticia SET visita_noticia = visita_noticia WHERE id_noticia = :id', 
            array
            (
                ':id'=>id
            ));
        }

        //* Metodos construtores
        public function __construct($_id='',$_idCategoria='',$_titulo='',$_img='',$_data='',$_ativo='',$_noticia='')
        {
            $this->id = $_id;
            $this->id_categoria = $_idCategoria;
            $this->titulo_noticia = $_titulo;
            $this->img_noticia = $_img;
            $this->data_noticia = $_data;
            $this->noticia_ativo = $_ativo;
            $this->noticia = $_noticia;
        }    
    }
?>