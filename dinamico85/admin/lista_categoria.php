<?php
require_once('conexao.php');
$query = "select * from categoria";
$cmd = $cn->prepare($query);
$cmd->execute();
$categorias_retornadas = $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count($categorias_retornadas)>0)
{
    // print_r ($categorias_retornadas);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista categoria</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="td_categoria" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
    <tr bgcolor="#993300" align="center">
        <th width="15%" height="2" font size="2" color="fff">Código</th>
        <th width="60%" height="2" font size="2" color="fff">Categoria</th>
        <th width="15%" height="2" font size="2" color="fff">Ativo</th>
        <th colspan="2"  font size="2" color="fff">Opções</th>
    </tr>
    <?php
    foreach($categorias_retornadas as $categoria){
    ?>
    <tr>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $categoria['id_categoria']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $categoria['categoria']?></font></td>
        <td><font size="2" face="verdana, arial" color="#00000"><?php echo $categoria['cat-ativo']=='1'?'sim':'não'; ?></font></td>
        <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Alterar</a></font></td>
        <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Excluir</a></font></td>
  </tr>
        <?php }} ?>
  </table>
</body>
</html>