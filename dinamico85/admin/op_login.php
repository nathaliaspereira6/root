<?php
require_once('../config.php');
$txt_login = isset($_POST['txt_login'])?$_POST['txt_login']:'';
$txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';
//echo $txt_login.' - '.$txt_senha;
if(empty($txt_login) || empty($txt_senha))  {
    //'Preencha os dados de Usuário.';
    header('Location: index.php?msg=Preencha os dados de Usuário');
    exit;
}
$adm = new Administrador();
$adm->efetuarlogin($txt_login,$txt_senha);
if(($adm->getId()==null))
{
    //'Usuario ou senha incorretos';
    header('Location: index.php?msg=Usuário ou senha incorretos');
    exit;
}

//registrando sessão do usuario
$_SESSION['logado'] = true;
$_SESSION['id_adm'] = $adm->getId();
$_SESSION['nome_adm'] =$adm->getNome();
$_SESSION['login_adm'] =$adm->getLogin();
header('Location: principal.php?link=');

//encerrando sessão de usuário
if($_GET['sair'])
{
    $_SESSION['logado'] = false;
    $_SESSION['id_adm'] = null;
    $_SESSION['nome_adm'] = null;
    $_SESSION['login_adm'] = null;
    header('Location: index.php');  
} 

?>