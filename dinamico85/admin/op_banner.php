<?php
if (isset($_POST['txt_banner'])){
    require_once('conexao.php');
    $banner = $_POST['txt_banner'];
    $link_banner = $_POST['txt_link'];
    $img_banner = $_POST['txt_img'];
    $alt = $_POST['txt_alt'];
    $ativa = isset($_POST['check_banner'])?'1':'0';
    $cmd = $cn->prepare("INSERT INTO banner (titulo_banner, link_banner,img_banner,alt,banner_ativo) VALUES (:cat,:catl,:cati,:cata,:ativ)");
    $cmd->execute(array(
        ':cat'=>$banner,
        ':catl'=>$link_banner,
        ':cati'=>$img_banner,
        ':cata'=>$alt,
        ':ativ'=>$ativa
    ));
    header('location:principal.php?link=8&msg=ok');
}

        //* Deletar banner
        $id = filter_input(INPUT_GET,'id');
        $excluir = filter_input(INPUT_GET,'excluir');
        if(isset($id)&& $excluir==1)
        {
            $adm = new Banner();
            $adm->setId($id);
            $adm->delete();
            header('location:principal.php?link=1&msg=ok');
        }
        //* Alterar banner
        if(isset($_POST['alterar']))
        {
            $adm = new Banner();
            $adm->update
            (
                $_POST['id'],
                $_POST['titulo_banner'],
                $_POST['link_banner'],
                $_POST['img_banner'],
                $_POST['alt'],
                $_POST['banner_ativo']
            );
            header('location:principal.php?link=2&msg=ok');
    }


?>