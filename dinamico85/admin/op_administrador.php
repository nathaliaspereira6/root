<?php
require_once('../config.php');
//inserir administrador
if (isset($_POST['cadastro']))
{
    $adm= new Administrador
    (
        $_POST['nome'],
        $_POST['email'],
        $_POST['login'],
        $_POST['senha']
    );
            $adm->insert();
            header('location:principal.php?link=11&msg=ok');
        }
        //* Deletar Administrador
        $id = filter_input(INPUT_GET,'id');
        $excluir = filter_input(INPUT_GET,'excluir');
        if(isset($id)&& $excluir==1)
        {
            $adm = new Administrador();
            $adm->setId($id);
            $adm->delete();
            header('location:principal.php?link=11&msg=ok');
        }
        //* Alterar administrador
        if(isset($_POST['alterar']))
        {
            $adm = new Administrador();
            $adm->update
            (
                $_POST['id'] ,
                $_POST['txt_nome'],
                $_POST['txt_email'],
                $_POST['txt_login']
            );
            header('location:principal.php?link=2&msg=ok');
    }
    

    if(isset($_POST['logar']))
    {
$txt_login = isset($_POST['txt_login'])?$_POST['txt_login']:'';
$txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';
//echo $txt_login.' - '.$txt_senha;


if(empty($txt_login) || empty($txt_senha))  {
    //'Preencha os dados de Usuário.';
    header('Location: index.php?msg=Preencha os dados de Usuário');
    exit;
}
$adm = new Administrador();
$adm->efetuarlogin($txt_login,$txt_senha);
if(($adm->getId()==null))
{
    //'Usuario ou senha incorretos';
    header('Location: index.php?msg=Usuário ou senha incorretos');
    exit;
}

//registrando sessão do usuario
$_SESSION['logado'] = true;
$_SESSION['id_adm'] = $adm->getId();
$_SESSION['nome_adm'] =$adm->getNome();
$_SESSION['login_adm'] =$adm->getLogin();
header('Location: principal.php?link=');
}
//encerrando sessão de usuário
if($_GET['sair'])
{
    $_SESSION['logado'] = false;
    $_SESSION['id_adm'] = null;
    $_SESSION['nome_adm'] = null;
    $_SESSION['login_adm'] = null;
    header('Location: index.php');  
} 

?>