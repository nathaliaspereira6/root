<?php
require_once('../config.php');
//inserir post
if (isset($_POST['cadastro']))
{
    $imagem = upload_imagem($_FILES["imagem"]);
    $post= new Post
    (
        $_POST['id_categoria_post'],
        $_POST['titulo_post'],
        $_POST['descricao_post'],
        $imagem[0],
        $_POST['data_post'],
        isset ($_POST['check_ativo'])?'1':'0'
    );
    
            $post->insert();
            header('location:principal.php?link=4&msg=ok');
}
        //* Deletar post
        $id = filter_input(INPUT_GET,'id');
        $excluir = filter_input(INPUT_GET,'excluir');
        if(isset($id)&& $excluir==1)
        {
            $post = new Post();
            $post->setId($id);
            $post->delete();
            header('location:principal.php?link=5&msg=ok');
        }
        //* Alterar post
        if(isset($_POST['alterar']))
        {
            $post = new Post();
            $post->update
            (
                $_POST['id'],
                $_POST['titulo_post'],
                $_POST['descricao_post'],
                $_POST['img_post'],
                $_POST['visitas'],
                $_POST['data_post']
            );
            header('location:principal.php?link=4&msg=ok');
        }
?>