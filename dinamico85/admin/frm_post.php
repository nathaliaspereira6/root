<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Cadastro de post</title>
</head>
<body>
    <h1>Novo post</h1>
    <form action="op_post.php" method="post" enctype="multipart/form-data" name="cadastro_form">
    <?php 
                    require_once('../config.php');
                    $cats = Categoria::getList();
                ?>
                <label for="">
                    <span>Categoria</span>
                    <select name="id_categoria_post" id="id_categoria_post" style="width: 100%; height: 30px; font-size: 15pt">            
                        <?php 
                            foreach ($cats as $cat) 
                            {
                                echo "<option value=".$cat['id_categoria'].">".$cat['categoria']."</option>";
                            }
                        ?>            
                    </select>
                </label>
        Titulo:<br>
        <input type="text" name="titulo_post" class="form-control"><br><br>
        Descrição:<br>
        <input type="text" name="descricao_post" class="form-control"><br><br>
        Data:<br>
        <input type="date" name="data_post" class="form-control"><br><br>
        Foto de exibição:<br>
        <input type="file" name="imagem" class="form-control"><br><br>
         Ativo:
        <input type="checkbox" name="check_ativo" id=""></p>
        <input type="submit" name="cadastro" value="Cadastrar post" class="btn btn-success" >
    </form>
    </body>
</html>