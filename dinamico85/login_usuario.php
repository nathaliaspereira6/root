<?php
include_once('../config.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login usuario</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="box-login">
        <div id="formulario-login">
            <form id="frmlogin" name="frmlogin" action="op_usuario.php" method="post">
                <fieldset>
                    <legend> Login </legend>
                    <br>
                    <label for=""><span>Email</span></label>
                    <input type="text" name="txt_email" id="txt_email">
                    <br>
                    <label for=""><span>Senha</span></label>
                    <input type="password" name="txt_senha" id="txt_senha">
                    <br>

                    <input type="submit" name="logar" id="logar" value="logar" class="botao">
                    <br>
                    <a href="frm_usuario.php?link=">Novo usuario</a>
                    <br>
                    <span><?php echo (isset($_GET ['msg']))?$_GET['msg']:"";?></span>
                </fieldset>
            </form>
        </div>
    </div>
</body>
</html>